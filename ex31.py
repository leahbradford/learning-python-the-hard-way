print "You enter a dark room with two doors. Do you go through door 1 or door 2?"

door = raw_input("> ")

if door == "1":
    print "There's a giant bear here eating a cake. What do you do?"
    print "1) Take the cake."
    print "2) Yell at the bear."

    bear = raw_input("> ")

    if bear == "1": # Ayyy nested if statements!
        print "The bear cries because you took its cake."
    elif bear == "2":
        print "The bear yells back and you do the reasonable thing and leave."
    else:
        print "Well, doing %s is probably better. Bear leaves with the cake." % bear

elif door == "2":
    print "1) You don't see anything, and close the door."
    print "2) You stare into an endless abyss."

    madness = raw_input("> ")

    if madness == "1":
        print "You survive this ordeal with the doors, but can no longer connect with other people emotionally."
    elif madness == "2":
        print "The abyss stares back."
    else:
        print "You enter the room despite the darkness."

else:
    print "You stumble around and fall, and you cannot get up. Just like in the commercial! "
