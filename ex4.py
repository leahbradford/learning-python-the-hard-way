cars = 100
# Sets car variable to 100.

space_in_a_car = 4.0
# Sets space_in_a_car variable to 4.0 (floating point).

drivers = 30
# Sets drivers variable to 30.

passengers = 90
# Sets passengers variable to 90.

cars_not_driven = cars - drivers
# cars_not_driven variable is equal to cars variable minus drivers variable (70).

cars_driven = drivers
# Sets cars_driven variable as equal to drivers variable (30).

carpool_capacity = cars_driven * space_in_a_car
# carpool_capacity variable is equal to cars_driven variable multiplied by space_in_a_car variable (120.0).

average_passengers_per_car = passengers / cars_driven
# average_passengers_per_car variable is equal to passengers variable divided by cars_driven variable (3).

print "There are", cars, "cars available."
# Prints "There are 100 cars available."

print "There are only", drivers, "drivers available."
# Prints "There are only 30 drivers available."

print "There will be", cars_not_driven, "empty cars today."
# Prints "There will be 70 empty cars today."

print "We can transport", carpool_capacity, "people today."
# Prints "We can transport 120.0 people today." .0 due to the floating decimal in the space_in_a_car variable (4.0).

print "We have", passengers, "to carpool today."
# Prints "We have 90 to carpool today."

print "We need to put about", average_passengers_per_car, "in each car."
# Prints "We need to put about 3 in each car."

# Study drill
# When the instructor wrote this program the first time he had a mistake, and Python told him about it like this:

# Traceback (most recent call last):
# File "ex4.py", line 8, in <module>
# average_passengers_per_car = car_pool_capacity / passenger
#NameError: name 'car_pool_capacity' is not defined

# Explain this error.

# Note to self, since we're not replicating the error, the initial code could look different.

# Line 46, I read about traceback and don't quite understand it yet.
# Line 47 shows the name of the file the error occurs in (ex4.py), and on what line number (line 8).
# Line 48 shows the line of code that caused the error (variable average_passengers_per_car).
# Line 49 states the "type" of the error (NameError - see error types in Python docs), and the error itself (that car_pool_capacity isn't defined).
# When car_pool_capacity "is not defined" that means it doesn't exist or doesn't have a value attributed to it ("it wasn't initialized" would be another way of saying this).
# average_passengers_per_car is attempting to use car_pool_capacity before before car_pool_capacity exists.
# TLDR variables must be defined before they are used.

# Other things that could've gone wrong:

# If a value is asigned to a function that can't perform that operation (like, let's say, division) a different error would be produced.

# I noticed "passenger" is not the variable name used in this exercise (it's "passengers"), so that could throw another error.

# Questions
# Why can't hyphens be used in variable names?
# What does <module> refer to in Python?
# What is a traceback?

# Additional reading
# Variable naming conventions: https://www.python.org/dev/peps/pep-0008/#function-and-variable-names
# Error/Exception types https://docs.python.org/2/library/exceptions.html (errors and exceptions are interchangeable terms)
