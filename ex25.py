def break_words(stuff):
    """This function will break up words for us."""
    words = stuff.split(" ") # Turns the sentence into a list.
    return words

def sort_words(words):
    """Sorts the words."""
    return sorted(words) # Sorts the list.

def print_first_word(words):
    """Prints the first word after popping it off."""
    word = words.pop(0) # Removes the first word in the list ("All").
    print word

# This function is both reading from and modifying the area, it's confusing.
# This is called a "side effect".
# The name of the function makes you think it does one thing, when it *also* does something else.
# I would prefer to call this print_and_remove_first_word.
# Having a function perform two separate actions is generally frowned upon.

def print_last_word(words):
    """Prints the last word after popping it off."""
    word = words.pop(-1) # Removes the last word in the list ("wait.").
    print word

def sort_sentence(sentence):
    """Takes in a full sentence and returns the sorted words."""
    words = break_words(sentence) # Sorts the list.
    return sort_words(words)

def print_first_and_last(sentence):
    """Prints the first and last words of the sentence."""
    print_first_word(words) # Prints the first word in the list ("All").
    print_last_word(words) # Prints the last word in the list ("who").

def print_first_and_last_sorted(sentence):
    """Sorts the words then prints the first and last one."""
    words = sort_sentence(sentence)
    print_first_word(words) # Prints the first word in the list ("All")
    print_last_word(words) # Prints the last word in the list ("who").

# Reading
# Using split http://www.pythonforbeginners.com/dictionary/python-split
# Sorting https://docs.python.org/2/howto/sorting.html
# Pop https://docs.python.org/2/tutorial/datastructures.html

# Questions
# Why use pop() rather than remove()?
# What's the difference between an array and a list in Python?

# Answers
# Pop looks at an index (a number) in the area, remove() finds a value (the actual thing, not where it is in the list) in the array and removes that.
# An array vs list, seems like an array is used to interface with other languages (arrays are the primary data structure in other languages), where list is used exclusively within Python.
