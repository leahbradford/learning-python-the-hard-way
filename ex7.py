print "Mary had a little lamb"

# Prints string, with %s format character being replaced with the string "snow".
print "Its fleece as white as %s" % 'snow'
print "And everywhere that Mary went"

# Adds 10 periods. Sounds like a nightmare.
print "." * 10

# Assigns strings to variables.
end1 = "C"
end2 = "h"
end3 = "e"
end4 = "e"
end5 = "s"
end6 = "e"
end7 = "B"
end8 = "u"
end9 = "r"
end10 = "g"
end11 = "e"
end12 = "r"

# This is going to add all the variables together and print them.
print end1 + end2 + end3 + end4 + end5 + end6,
print end7 + end8 + end9 + end10 + end11 + end12

# Exercises like this are why I'm going to have carpal tunnel.

# Note: it's bad form to make lines in Python that are longer than 80 characters.
