# Study drill 3
# Combine raw_input with argv to make a script that gets more input from a user.

from sys import argv

breakfast_options, one, two, three = argv

raw_input("Those are your top 3 picks for brunch, huh? ")
print "The script is called:", breakfast_options
print "Your first variable is:", one
print "Your second variable is:", two
print "Your third variable is:", three

# A more complex version could be:
# The script could expect 3 types of brunch, and if the user passes fewer than 3, use raw_input() to fill in the missing ones, rather than throwing an error.

# A good example of argv working with raw_input() is git commits.
# When you type "git commit" without a commit message, it pops open a text editor and asks for the commit message.
