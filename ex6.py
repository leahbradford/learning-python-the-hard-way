# A variable with a string value, and the format character %d that is replaced with the value of "10".
x = "There are %d types of people" % 10

# A variable with a string value of "binary".
binary = "binary"

# A variable with a string value of "don't".
do_not = "don't"

# A variable with a string value, and two %s format characters, which are replaced with the variables "binary" and "do not", respectively.
y = "Those who know %s and those who %s." % (binary, do_not)

# Prints variables x and y.
print x
print y

# Prints strings with %r and %s format characters, which are replaced with the value of variables x and y.
print "I said %r." % x
print "I also said: '%s'." % y

# A variable with a false Boolean value.
hilarious = False

# A variable with a string value and a %r format character, which will be replaced with the variable "hilarious".
joke_eval = "Isn't that joke so funny?! %r"

# Prints variable
print joke_eval % hilarious

# Variables w and e are assigned string values, each are half a sentence.
w = "This is the left side of..."
e = "a string with a right side."

# Prints the combination of variables w and e.
print w + e

# Note: %r is used for debugging, as it displays the "raw" data of the variable, other formatters are used to display data to users.
