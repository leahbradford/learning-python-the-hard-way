# Study drill 1
# Explain the below error.

# $ python ex13.py first 2nd
# Traceback (most recent call last):
# File "ex13.py", line 3, in <module>
# script, first, second, third = argv
# ValueError: need more than 3 values to unpack

# Line 4, there's two arguments being passed into argv: "first", and "2nd".
# Line 5, I still don't understand what traceback is.
# Line 6 shows the file name and line number the error is occuring on.
# Line 7 shows the code that is causing the error.
# Line 8 shows the error type (ValueError), and explains that it has received an incorrect value (it's received 3 values where it expected 4).

# Questions
# Referencing line 8, why does the ValueError state "need more than 3 values to unpack", when it needs exactly 3. This is pretty misleading.
