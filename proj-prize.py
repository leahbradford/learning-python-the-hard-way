import random

for which_prize in range(1):
    random_number = random.randint(0, 200)
    print random_number

    if random_number <= 50:
        print "You win a wooden rabbit!"
    elif random_number <= 150:
        print "You win nothing, sorry!"
    elif random_number <= 180:
        print "You win a box of wafer thin mints, congrats!"
    else:
        print "You win a REAL LIVE PENGUIN."
        print "Just kidding, it's a stuffed animal."
