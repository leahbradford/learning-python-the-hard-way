# This is a comment.

print "I could have code like this." # and the comment after is ignored

# Comments can also "disable" or comment out a piece of code:
# print "This won't run."

print "This will run."
