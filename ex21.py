# Adds parameters a and b (gets passed 30, 5)
def add(a, b):
    print "Adding %d + %d" % (a, b)
    return a + b # returns function

# Subtracts parameters a and b (gets passed 78, 4)
def subtract(a, b):
    print "Subtracting %d - %d" % (a, b)
    return a - b

# Multiplies a and b (gets passed 90, 2)
def multiply(a, b):
    print "Multiplying %d * %d" % (a, b)
    return a * b

# Divides a and b (gets passed 100, 2)
def divide(a, b):
    print "Dividing %d / %d" % (a, b)
    return a / b

print "let's do some math with just functions!"

age = add(30, 5) # Calls function with two arguments: a and b.
height = subtract(78, 4)
weight = multiply(90, 2)
iq = divide(100, 2)

print "Age: %d, Height: %d, Weight: %d, IQ: %d" % (age, height, weight, iq)

print "Here's a puzzle."
what = add(age, subtract(height, multiply(weight, divide(iq, 2))))
# Divides iq (50) by 2 (25)
# Multiplies weight (180) by 25 (4500)
# Subtracts height (74) by 4500 (-4426)
# Adds age (35) to 4426 (4391)

print "That becomes: ", what, "Can you do it by hand?"

# Think of return like print, but for programs.
# So a function performs some action, then returns the result so it (the result) can be used by another program(s).

# Reading
# Return statement https://docs.python.org/2/reference/simple_stmts.html#grammar-token-return_stmt
