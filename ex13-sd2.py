# Study drill 2
# Write a script that has fewer arguments and one that has more.

from sys import argv

# Fewer arguments:
# script, apple, banana = argv
# print "The script is called:", script
# print "Your first variable is:", apple
# print "Your second variable is:", banana

# More arguments:
script, blue_crayon, red_crayon, yellow_crayon, green_crayon = argv
print "The script is called:", script
print "Your first variable is:", blue_crayon
print "Your second variable is:", red_crayon
print "Your third variable is:", yellow_crayon
print "Your fourth variable is:", green_crayon
