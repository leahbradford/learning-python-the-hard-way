# Imports argv from module sys
from sys import argv

# Arguments being passed into argv
script, input_file = argv

# Functions
# Passes current_file as f and reads its contents
def print_all(f):
    print f.read()

# Passes current_file as f and goes to the first line
def rewind(f):
    f.seek(0) # Starts reading from byte 0

# Passes current_line and current_file as line_count and f and reads the current line
def print_a_line(line_count, f):
    print line_count, f.readline(),

# Opens file.
current_file = open(input_file)

# Reads and prints file contents
print "First let's print the whole file: \n"
print_all(current_file)

# Goes back to the first line
print "Now let's rewind, kind of like a tape."
rewind(current_file) # Be kind, please rewind!

# Prints the 3 lines of the file
print "let's print three lines:"

current_line = 1
print_a_line(current_line, current_file) # Prints line 1

current_line += 1
print_a_line(current_line, current_file) # Prints line 2

current_line += + 1
print_a_line(current_line, current_file) # Prints line 3

# Reading:
# Inputs and outputs https://docs.python.org/2/tutorial/inputoutput.html
# Functions https://docs.python.org/2/library/functions.html#file
