def cheese_and_crackers(cheese_count, boxes_of_crackers):
    print "You have %d cheeses!" % cheese_count
    print "You have %d boxes of crackers!" % boxes_of_crackers
    print "That's enough for a party!"
    print "We better get a blanket. \n"

# Line 9 passes arguments (20 and 30) to the cheese_and_crackers function.
print "We can just give the functions numbers directly:"
cheese_and_crackers(20, 30)

# Lines 13 and 14 assign values to variables "amount_of_cheese" and "amount_of_crackers".
print "OR, we can use variables from our script:"
amount_of_cheese = 10
amount_of_crackers = 50

# Then line 17 passes those variables (arguments) to the function.
cheese_and_crackers(amount_of_cheese, amount_of_crackers)

# Line 21 passes the solution to these equations (30 and 11, the arguments) to the function.
print "We can even do math inside too:"
cheese_and_crackers(10 + 20, 5 + 6)

# Line 26 adds the value of amount_of_cheese and 100 (110), and amount_of_crackers and 1000 (1050), then passes these arguments to the function.
# C-c-c-combo breaker!
print "And we can combine the two, variables and math:"
cheese_and_crackers(amount_of_cheese + 100, amount_of_crackers + 1000)
