# This is the import statement.
from sys import argv

# An import statement is how you add modules to your script (this file) from Python.

# Import statements work in two steps: (1) find a module, and initialize it if necessary; (2) reference a name (or names) within the local name space (where the import statement occurs, which is this file).

# So on line 2, sys is the module, and argv is being referenced from within sys. You could think of it like copying that code into this file.

# Note that a module can be any .py file.

# This shows what arguments are contained in argv.
script, first, second, third = argv

# argv is a list of arguments ("arguments" in this case meaning variables), and is expected to contain at least one value: the script name (which would be the first variable, "script"). How many arguments you include (first, second, third) are optional.

# What argv does is hold the arguments you pass to your Python script (in the command line) when you run the script.

# For example:
# When I write "python ex13.py" in the command line, "ex13.py" is the script name, and the argument.

# Lines 21-24 "unpack" argv, so that the values that pass through it get assigned to four variables: script, first, second and third.
print "The script is called:", script
print "Your first variable is:", first
print "Your second variable is:", second
print "Your third variable is:", third

# Think of it like:
# script = argv[0]
# first = argv[1]
# second = argv[2]
# third = argv[3]

# Reading
# Import statements https://docs.python.org/2.0/ref/import.html
# Python modules https://docs.python.org/2/tutorial/modules.html
# Python set module https://docs.python.org/2/library/sets.html
# Review errors/built-in exceptions https://docs.python.org/2/library/exceptions.html
