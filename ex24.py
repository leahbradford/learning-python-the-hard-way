print "Let's practice everything."

# Tabs, new line, and escape characters
print 'You\'d need to know \'bout escapes with \\ that do \n new lines and \t tabs.'

# Paragraph string
poem = """
\t The lovely world
with logic so firmly planted
cannot discern \n the needs of love
nor comprehend passion from intuition
and requires an explanation
\n\t\t where there is none.
"""

# Prints the poem variable
print "------------"
print poem
print "------------"

# Basic math
five = 10 - 2 + 3 - 6
print "This should be five: %s" % five

# Function
def secret_formula(started):
    jelly_beans = started * 500
    jars = jelly_beans / 1000
    crates = jars / 100
    return jelly_beans, jars, crates
# Reminder: the variable names inside of functions are temporary.

# Passes 10,000 into "started"
start_point = 10000
one, two, three = secret_formula(start_point)
# The variables here will be passed the values from the function, in order.

print "With a starting point of: %d" % start_point
print "We'd have %d beans, %d jars, and %d crates." % (one, two, three) # Prints the values of variables one, two, three
# Answers are 5,000,000 beans, 5,000 jars, 50 crates

# Passes 1,000 into "started"
start_point = start_point / 10

print "We can also do that this way:"
print "We'd have %d beans, %d jars, and %d crates." % secret_formula(start_point)
# Prints the values of the variables in the function in order.
# Answers are 50,000 beans, 500 jars, 5 crates
