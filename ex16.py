from sys import argv

script, filename = argv

print "We're going to ease %r." % filename
print "If you don't want that, hit CTRL-C."
print "If you do want that, hit RETURN."

raw_input("?")

print "Opening the file..."
target = open(filename, 'w')
# Opens the file in "write" mode. Write mode truncates the file first (meaning it deletes everything in the file, without deleting the file itself).

print "...and truncating the file, bye!"
# target.truncate()
# Opening in write mode makes target.truncate() redundant.

print "Now I'm going to ask you for three lines."

line1 = raw_input("line 1: ")
line2 = raw_input("line 2: ")
line3 = raw_input("line 3: ")

print "I'm going to write these to the file."

# line =  line1 + '\n' + line2 + '\n' + line3 + '\n'
# Not the most performant way, as strings are immutable (meaning they can't be edited, only overwritten). This way it has to be overwritten with each new line.

line = "%r\n%r\n%r\n" % (line1, line2, line3)
# More performant, as it generates a single string.

target.write(line)

print "And finally, we close it."
target.close()
