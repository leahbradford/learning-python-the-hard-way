print "I will now count my chickens:" # Prints string.

print "Hens", 25+30/6 # Prints string and equation result (dividing 30 by 6 then adding 25).
print "Roosters", 100-25*3%4 # Prints string and equation result.

# When it comes to modulo, the concept is "X divided by Y with J remaining" i.e. 100 divided by 16 with 4 remaining.

# The data type is called "integer division".
# Integer division removes the remainder, so 100/16 = 6 (it seems Python does this by default).
# Modulo gives you the remainder that wasn't accounted for in integer division, so 4.

# See Python data types: https://docs.python.org/2/library/stdtypes.html

print "Now I will count the eggs:" # Prints string.

print 3+2+1-5+4%2-1/4+6 # Prints equation result.

print "Is it true that 3+2 < 5-7?" # Prints string.

print 3+2 < 5-7 # Prints the equation result (a Boolean type), and casts as a string (with an answer of False, as 5 is not less than -2).

# "Casting" is the act of changing one data type to another data type.

print "What is 3+2?", 3+2 # Prints string and equation result.

print "What is 5-7?", 5-7 # Prints string and equation result.

print "Oh, that's why it's False." # Prints string.

print "How about some more..." # Prints string.

# Prints string and casts the equation result (Boolean) as a string.
print "Is it greater?", 5 > -2
print "Is it greater or equal?", 5 >= -2
print "Is it less or equal?", 5 <= -2

# Study drill: use floating point numbers.
# Python docs https://docs.python.org/2/tutorial/floatingpoint.html

# Prints equation results.
print 20.0/6
print 7.0/4.0
print 7/4

# Questions:
# Are comments usually written following sentence structure, or all lower case (is there a rule)?
# When an equation is being printed, is it as a string or integer? I assume it's a string, but am unsure.
# Why does Python drop the fraction after the decimal (unless you specify having the floating point)?
