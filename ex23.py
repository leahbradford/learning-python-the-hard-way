# Exercise 23 is to read some code.

# Avoid any project that mentions “Python 3.”

# Go to bitbucket.org, github.com, or gitorious.org with your favorite web browser and search for “python.”

# Pick a random project.

# Go to the Source tab and browse through the list of files and directories until you find a .py file (but not setup.py).

# Start at the top and read through it, taking notes on what you think it does.

# If any symbols or strange words seem to interest you, write them down to research later.
