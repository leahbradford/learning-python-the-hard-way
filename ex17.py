from sys import argv
# Imports argv from sys module.
from os.path import exists
# Imports exists from os.path.
# os is the module, and path is the scope within it.

script, from_file, to_file = argv
# Variables that'll be assigned values through argv.

print "Copying from %s to %s" % (from_file, to_file)

indata = open(from_file).read()
# Opens and reads from_file (the name of the file being copied).

# in_file = open(from_file)
# indata = in_file.read()
# Redundant code from the lesson.

print "The input file is %d bytes long" % len(indata)

print "Does the output file exist? %r" % exists(to_file)
print "Ready, hit RETURN to continue, CTRL-C to abort."
raw_input()

out_file = open(to_file, 'w')
# Opens the file in write mode, which also truncates the file.

out_file.write(indata)
# Writes the content of indata to out_file.

print "Alright, all done."

out_file.close()
# Closes out_file
