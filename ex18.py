# Similar to a "script = argv" statement
def print_two(*args):
    arg1, arg2 = args
    print "arg1: %r, arg2: %r" % (arg1, arg2)

# def defines a function in Python.
# *args needs to be inside () parens to work.
# The first function line ends with a colon.

# The * in *args tells Python to take all the arguments to the function, then put them in args as a list. It's like argv, but for functions.

# *args is unecessary, so the above code can be rewritten to this.
def print_two_again(arg1, arg2): # These variables are called "parameters"
    print "arg1: %r, arg2: %r" % (arg1, arg2)
# Line 13 prints "Leah", and "Bradford" as dictated on line 27.

# This function takes one argument.
def print_one(arg1):
    print "arg1 %r" % arg1 # This will print "First!"

# This takes no parameters, as none are named or are being called (line 29).
def print_none():
    print "I got nuffin'."

# Calls the functions with parameters
print_two("Leah", "Bradford") # These are the "arguments" (values).
print_two_again("Leah", "Bradford")
print_one("First!")
print_none()

# Reading
# Function definitions https://docs.python.org/2.0/ref/function.html
# More on function definitions https://www.learnpython.org/en/Functions
